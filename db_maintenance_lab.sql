-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2023 at 04:58 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_maintenance_lab`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_history`
--

CREATE TABLE `tbl_history` (
  `id_penanggung_jawab` varchar(11) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_history`
--

INSERT INTO `tbl_history` (`id_penanggung_jawab`, `tanggal`, `keterangan`) VALUES
('SWS001', '2023-05-08', '123 melakukan login'),
('SWS001', '2023-05-08', 'sompie melakukan penambahan user'),
('SWS001', '2023-05-08', '123 melakukan penambahan user dengan nama 123'),
('PGJ003', '2023-05-08', '123 melakukan penambahan user dengan nama sompie12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_iventory`
--

CREATE TABLE `tbl_iventory` (
  `id_alat` varchar(11) NOT NULL,
  `jenis_alat` varchar(11) NOT NULL,
  `jumlah_alat` int(11) NOT NULL,
  `merk_dan_model_alat` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_komputer`
--

CREATE TABLE `tbl_komputer` (
  `id_komputer` varchar(11) NOT NULL,
  `mouse` varchar(40) NOT NULL,
  `keyboard` varchar(40) NOT NULL,
  `versi_OS` varchar(20) NOT NULL,
  `monitor` varchar(40) NOT NULL,
  `status_komputer` varchar(20) NOT NULL,
  `id_lab` varchar(11) NOT NULL,
  `id_penanggung_jawab` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_penanggung_jawab`
--

CREATE TABLE `tbl_penanggung_jawab` (
  `id_penanggung_jawab` varchar(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `jurusan` varchar(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `userlevel` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_penanggung_jawab`
--

INSERT INTO `tbl_penanggung_jawab` (`id_penanggung_jawab`, `nama`, `kelas`, `jurusan`, `username`, `password`, `userlevel`) VALUES
('PGJ001', 'sompie', 'Kelas-11', 'RPL', '521', '521', 'Siswa'),
('PGJ002', 'sompie123', 'Kelas-11', 'RPL', '451', '451', 'Siswa'),
('PGJ003', '123', 'Kelas-10', 'TIK-1', '123', '123', 'Siswa'),
('PGJ004', 'sompie123', 'Kelas-10', 'TKJ', '123', '123', 'Siswa'),
('SWS001', '123', '123', '123', '123', '123', 'Siswa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_history`
--
ALTER TABLE `tbl_history`
  ADD KEY `id_penanggung_jawab` (`id_penanggung_jawab`);

--
-- Indexes for table `tbl_iventory`
--
ALTER TABLE `tbl_iventory`
  ADD PRIMARY KEY (`id_alat`);

--
-- Indexes for table `tbl_komputer`
--
ALTER TABLE `tbl_komputer`
  ADD PRIMARY KEY (`id_komputer`),
  ADD KEY `penanggung_jawab` (`id_penanggung_jawab`),
  ADD KEY `id_penanggung_jawab` (`id_penanggung_jawab`);

--
-- Indexes for table `tbl_penanggung_jawab`
--
ALTER TABLE `tbl_penanggung_jawab`
  ADD PRIMARY KEY (`id_penanggung_jawab`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_history`
--
ALTER TABLE `tbl_history`
  ADD CONSTRAINT `tbl_history_ibfk_1` FOREIGN KEY (`id_penanggung_jawab`) REFERENCES `tbl_penanggung_jawab` (`id_penanggung_jawab`);

--
-- Constraints for table `tbl_komputer`
--
ALTER TABLE `tbl_komputer`
  ADD CONSTRAINT `tbl_komputer_ibfk_1` FOREIGN KEY (`id_penanggung_jawab`) REFERENCES `tbl_penanggung_jawab` (`id_penanggung_jawab`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
